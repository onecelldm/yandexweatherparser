#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <QSettings>
#include <QColorDialog>
#include <parser.h>
#include <network.h>
namespace Ui {
class settingsDialog;
}

class settingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit settingsDialog(QWidget *parent = nullptr);
    explicit settingsDialog(QWidget *parent = nullptr,QSettings *Qs=nullptr );
    void readData();
    void save();

    ~settingsDialog();

private slots:
    void on_buttonBox_accepted();


    void on_pushButton_clicked();

    void on_spinBox_valueChanged(const QString &arg1);

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_comboBox_activated(int index);

private:
    QSettings *settings;

     int PosX, PosY,selectIndex;
    QString city,TextColor,TextSize,URL;
    QStringList listCityUrls;
    bool Pin;
    Parser parser;
    Network network;

    Ui::settingsDialog *ui;
};

#endif // SETTINGSDIALOG_H
