#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include <qdebug.h>
#include <parser.h>
#include <QNetworkAccessManager>
#include <QtNetwork>
#include <QTextCodec>
settingsDialog::settingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::settingsDialog)
{
    ui->setupUi(this);
}

settingsDialog::settingsDialog(QWidget *parent,QSettings *settingsvalue) :
    QDialog(parent),
    ui(new Ui::settingsDialog)
{   settings=settingsvalue;
      readData();
    ui->setupUi(this);
    ui->spinBox->setValue(TextSize.replace("px","").toInt());
    ui->label_3->setText("Определяеться "+city+"\nПоиск других городов");


}

settingsDialog::~settingsDialog()
{
    delete ui;
}
void settingsDialog::readData()
{


      URL=settings->value("city/url","https://yandex.ru/pogoda/moscow").toString();
      TextColor=settings->value("text/text-color","#000000").toString();
      TextSize=settings->value("text/size","12px").toString();
      Pin=settings->value("pinned","false").toBool();
      city=parser.CityNameParser(network.GetData(URL));
}

void settingsDialog::save()
{

    settings->setValue("city/url",URL);
    settings->setValue("text/text-color",TextColor);
    settings->setValue("text/size",TextSize);
    settings->setValue("pinned",Pin);
    settings->sync();
    qDebug()<<"sync ok";

}


void settingsDialog::on_buttonBox_accepted()
{

    //p.CityNameParser);
    save();

}

void settingsDialog::on_pushButton_clicked()
{
   QColor color = QColorDialog::getColor(Qt::black, this, "Pick a color",  QColorDialog::DontUseNativeDialog);
   if (!color.isValid() ) {

   }
   else{
    TextColor=color.name();
    ui->label_2->setStyleSheet("font-size:"+TextSize+"; color: "+TextColor);
    ui->label->setStyleSheet("font-size:"+TextSize+"; color: "+TextColor);

   }


}



void settingsDialog::on_spinBox_valueChanged(const QString &arg1)
{    TextSize=arg1+"px";
     ui->label_2->setStyleSheet("font-size:"+TextSize+"; color: "+TextColor);
     ui->label->setStyleSheet("font-size:"+TextSize+"; color: "+TextColor);
}

void settingsDialog::on_pushButton_2_clicked()
{
    if(ui->lineEdit->text()!=""&ui->lineEdit->text()!=" ")
    {
        Parser parser;
        QString req= "https://yandex.ru/pogoda/search?request="+ui->lineEdit->text();
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

        std::map<QString,QStringList> dataMap=parser.citiesParser(network.GetData( req));
        listCityUrls=dataMap["urls"];
        ui->comboBox->clear();
        if(dataMap["titles"].count()>0){
            for (int i=0; i<dataMap["titles"].count(); i++ ) {
                ui->comboBox->addItem(dataMap["titles"].at(i));

            }
        }
        else{
            ui->comboBox->addItem("Нет результатов");
        }
        selectIndex=0;



    }
    else{

    }


}

void settingsDialog::on_pushButton_3_clicked()
{


    if(listCityUrls.count()>0){
     URL=network.GetForwardUrl( listCityUrls.at(selectIndex));

    }
}

void settingsDialog::on_comboBox_activated(int index)
{
    selectIndex=index;
    if(listCityUrls.count()>0) qDebug()<<"Вы выбрали"+listCityUrls.at(selectIndex);
}
