#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QTextCodec>
#include <QNetworkAccessManager>
#include <QObject>
#include <networkexception.h>
#include  <QSettings>


#include <QFile>
#include "parser.h"
#include <QtSvg>
#include <exception>
#include <QtNetwork>
#include <QPainter>
#include <QImage>
#include <settingsdialog.h>
#include <network.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QByteArray Get(QString urtl);
    QImage SvgToImage(QString Path);


    void SetSettings(), init(),SettingsRead();



private slots:

    void closeEvent(QCloseEvent *event);
    void ShowContextMenu(const QPoint &pos);
    void opensettings();
    void ShowData();
    void PinUnpinWindow();

private:


    QMenu contextMenu;

    QAction AenablePin,AdisablePin,OpenSettings, Aclose;
    bool started=false;
    QTimer  *timer = new QTimer();
    QSettings *settings;
    int PosY,PosX,width,height;
    QString city,URL,URLDETAILS, TextColor,Textsize;
    settingsDialog *dialog;
    bool Pin;
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
