#include "network.h"

Network::Network(QObject *parent) : QObject(parent)
{

}

QString Network::GetForwardUrl(QString url)
{
    QNetworkRequest request= QNetworkRequest (QUrl(url));
     request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
     request.setRawHeader( "User-Agent" , "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0" );
    QNetworkAccessManager *mngr = new QNetworkAccessManager(this);
    QNetworkReply  *response=mngr->get(request);
    QEventLoop event;
    connect(response,SIGNAL(finished()),&event,SLOT(quit()));
    event.exec();
    if(response->error()==QNetworkReply::NoError)
    {
       QString dataReturn=response->url().toString();
       mngr->deleteLater();
       response->deleteLater();
        return dataReturn;
    }
    else{
        throw  NetworkException(response);

    }
}
QByteArray  Network::GetData(QString url)
{

    QNetworkRequest request= QNetworkRequest (QUrl(url));
     request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
     request.setRawHeader( "User-Agent" , "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0" );
    QNetworkAccessManager *mngr = new QNetworkAccessManager(this);
    QNetworkReply  *response=mngr->get(request);
    QEventLoop event;
    connect(response,SIGNAL(finished()),&event,SLOT(quit()));
    event.exec();

    if(response->error()==QNetworkReply::NoError)
    {
        QByteArray dataReturn= response->readAll();
        mngr->deleteLater();
        response->deleteLater();
        return dataReturn;

    }
    else{
        throw  NetworkException(response);
    }

}
QString   Network::Download(QString url,QString fileName)
{

    QByteArray arry= GetData(url);
    QFile *f =new QFile(fileName);
    if(f->exists())
    {
        f->remove();
    }
    if(f->open(QFile::Append))
    {
        f->write(arry);
        f->close();
        delete f;
        return fileName;
    }
}
/*
bool Network::CheckConnection(QString url)
{
    qDebug()<<url;
    QNetworkRequest request= QNetworkRequest (QUrl(url));
     request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
     request.setRawHeader( "User-Agent" , "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0" );
    QNetworkAccessManager *mngr = new QNetworkAccessManager(this);
    QNetworkReply  *response=mngr->get(request);
    QEventLoop event;
    connect(response,SIGNAL(finished()),&event,SLOT(quit()));
    event.exec();

    qDebug()<<response->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if(response->error()==QNetworkReply::NoError)
    {

    return  true;
    }
    else{
        return  false;
    }
}
*/

