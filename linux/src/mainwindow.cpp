#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <exception>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qDebug() << QSslSocket::supportsSsl()
             << QSslSocket::sslLibraryBuildVersionString()
             << QSslSocket::sslLibraryVersionString();
    init();
    ShowData();
}
void MainWindow::init()
{
    QCoreApplication::setOrganizationName("seld");
    QCoreApplication::setApplicationName("app");
    QApplication::setQuitOnLastWindowClosed( false );
    setWindowFlags(Qt::Widget );//| Qt::FramelessWindowHint);
    setAttribute(Qt::WA_NoSystemBackground, true);
    setAttribute(Qt::WA_TranslucentBackground, true);
    setContextMenuPolicy(Qt::CustomContextMenu);
    contextMenu.setTitle(tr("Context menu"));

     AenablePin.setText("Закрепить Виджет");
     AdisablePin.setText("Открепить Виджет");
     OpenSettings.setText("Открыть настройки");
     Aclose.setText("Закрыть");

    connect(&AenablePin, SIGNAL(triggered()), this, SLOT(PinUnpinWindow()));
    connect(&AdisablePin, SIGNAL(triggered()), this, SLOT(PinUnpinWindow()));
    connect(&OpenSettings, SIGNAL(triggered()), this, SLOT(opensettings()));
    connect(&Aclose, SIGNAL(triggered()), this, SLOT( close()));
    connect(this, SIGNAL(customContextMenuRequested(const QPoint &)),
            this, SLOT(ShowContextMenu(const QPoint &)));
    connect(timer, SIGNAL(timeout()), this, SLOT(ShowData()));

    SettingsRead();
    SetSettings();
     //timer->start(3000);
     timer->start(900000);
}
void MainWindow::SettingsRead()
{



  settings = new QSettings("YaWeatherSettings", QSettings::NativeFormat);
  width=settings->value("width","600").toInt();
  height=settings->value("height","400").toInt();
  PosX=settings->value("position/x","0").toInt();
  PosY =settings->value("position/y","100").toInt();

  TextColor=settings->value("text/text-color","#000000").toString();
  qDebug()<<"clor:"+TextColor;
  Textsize=settings->value("text/size","12px").toString();
  Pin=settings->value("pinned","false").toBool();
  URL=settings->value("city/url","https://yandex.ru/pogoda/moscow?").toString();





}
void MainWindow::SetSettings()
{


   ui->LabelStatus->setStyleSheet("font-size:"+Textsize+"; color: "+TextColor);

   ui->LabelCorrentTemp->setStyleSheet("font-size:"+Textsize+"; color: "+TextColor);
   ui->LabelCorrentImage->setStyleSheet("font-size:"+Textsize+"; color: "+TextColor);
   ui->LabelMorningTemp->setStyleSheet("font-size:"+Textsize+"; color: "+TextColor);
   ui->LabelMorningImage->setStyleSheet("font-size:"+Textsize+"; color: "+TextColor);
   ui->LabelDayTemp->setStyleSheet("font-size:"+Textsize+"; color: "+TextColor);
   ui->LabelDeyImage->setStyleSheet("font-size:"+Textsize+"; color: "+TextColor);
   ui->LabelEvenignTemp->setStyleSheet("font-size:"+Textsize+"; color: "+TextColor);
   ui->LabelEveningImage->setStyleSheet("font-size:"+Textsize+"; color: "+TextColor);
   ui->LabelNightTemp->setStyleSheet("font-size:"+Textsize+"; color: "+TextColor);
   ui->LabelNightImage->setStyleSheet("font-size:"+Textsize+"; color: "+TextColor);
   if(started==false)
   {
    this->setGeometry(PosX,PosY,width,height);
       PinUnpinWindow();
       started=true;

   }


}

void MainWindow:: ShowData()
{
    try {
        ui->LabelCorrentImage->setText("");
        ui->LabelCorrentTemp->setText("Сейчас:");
        ui->LabelMorningTemp->setText("Утро: " );
        ui->LabelDayTemp->setText("День: ");
        ui->LabelEvenignTemp->setText("Вечер: ");
        ui->LabelNightTemp->setText("Ночь:");

        ui->LabelMorningImage->setText("");
        ui->LabelDeyImage->setText("");
        ui->LabelEveningImage->setText("");
        ui->LabelNightImage->setText("");
        Network network;
        Parser parse;

        if(URL.split('?').count()>1)
        {
            URLDETAILS=URL.split("?")[0];
            if(URL.split("?")[0].right(1)!="/")
            {
            URLDETAILS+="/";
            }
            URLDETAILS+="details?"+URL.split("?")[1];
        }
        else{ URLDETAILS=URL.split("?")[0]+"/details?";}


        QByteArray Data= network.GetData(URL);
        city=parse.CityNameParser(Data);
        ui->LabelStatus->setText(city);

         //parse->UfoParser(Get(URL));
        std::map<QString,QString> MP= parse.CorrentDataParse(Data);
        ui->LabelCorrentTemp->setText("Сейчас: "+ MP.at("temp"));

        ui->LabelCorrentImage->setPixmap(QPixmap::fromImage(SvgToImage(network.Download(MP.at("img"),"I1.svg"))));

        QByteArray DetailsData=network.GetData(URLDETAILS);
        MP=parse.TempParser(DetailsData);

        ui->LabelMorningTemp->setText("Утро: "+ MP.at("morning"));
        ui->LabelDayTemp->setText("День: "+ MP.at("day"));
        ui->LabelEvenignTemp->setText("Вечер: "+MP.at("evening"));
        ui->LabelNightTemp->setText("Ночь: "+ MP.at("night"));


        MP=parse.ImageParser(DetailsData);

        ui->LabelMorningImage->setPixmap(QPixmap::fromImage(SvgToImage(network.Download(MP.at("morning"),"morning.svg"))));
        ui->LabelDeyImage->setPixmap(QPixmap::fromImage(SvgToImage(network.Download(MP.at("day"),"day.svg"))));
        ui->LabelEveningImage->setPixmap(QPixmap::fromImage(SvgToImage(network.Download(MP.at("evening"),"evening.svg"))));
        ui->LabelNightImage->setPixmap(QPixmap::fromImage(SvgToImage(network.Download(MP.at("night"),"night.svg"))));



    }
    catch(NetworkException Ne){
        QMessageBox qm;
        qm.setText("NetworkError:"+Ne.GetMessageQString());
        qm.addButton(QMessageBox::Retry);
        qm.addButton(QMessageBox::Cancel);
        if(qm.exec()==QMessageBox::Retry)
        {
            ShowData();
        }
        else{QApplication::exit();}

    }
    catch(std::exception e){
         ShowData();
    }


}

void MainWindow::PinUnpinWindow()
{
    QRect gem= geometry();
    hide();
    if(!started)
    {
        if(Pin)
            setWindowFlags(Qt::Tool | Qt::WindowStaysOnBottomHint|Qt::CustomizeWindowHint);
        else setWindowFlags(Qt::Widget );

    }
    else{
        if(Pin)
        {
        setWindowFlags(Qt::Widget );
        Pin=false;
        }
        else
        {
        setWindowFlags(Qt::Tool| Qt::WindowStaysOnBottomHint|Qt::CustomizeWindowHint);
        Pin=true;
        }
    }
    setGeometry(gem);
    show();

    settings->setValue("pinned",Pin);
    settings->sync();
}


void MainWindow::opensettings()
{
    dialog=new settingsDialog(this,settings);
    if(dialog->exec()==QDialog::Accepted)
    {
        SettingsRead();
        SetSettings();
        ShowData();
    }
}


void MainWindow::ShowContextMenu(const QPoint &pos)
{
    contextMenu.clear();
   if(!Pin){
   contextMenu.addAction(&AenablePin);
   }
   else{
   contextMenu.addAction(&AdisablePin);
   }
   contextMenu.addAction(&OpenSettings);

   contextMenu.addAction(&Aclose);
   contextMenu.exec(mapToGlobal(pos));
}

QImage MainWindow::SvgToImage(QString Path)
{
       QSvgRenderer renderer(Path);
       QImage image(100, 100, QImage::Format_ARGB32);
       image.fill(QColor(0,0,0,0));
       QPainter painter(&image);
       renderer.render(&painter);
       return  image;

}

void MainWindow::closeEvent(QCloseEvent *event)
{
    PosX=this->geometry().x();
    PosY=this->geometry().y();
    width=this->geometry().width();
    height=this->geometry().height();
    settings->setValue("position/x",PosX);
    settings->setValue("position/y",PosY);
    settings->setValue("width",width);
    settings->setValue("height",height);
    settings->sync();
    QApplication::exit();
}
MainWindow::~MainWindow()
{
    delete ui;
}

