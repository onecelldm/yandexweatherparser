#include "networkexception.h"

NetworkException::NetworkException(QNetworkReply *reply)
{
    message=reply->errorString();
    reply->deleteLater();

}
QString NetworkException ::GetMessageQString()
{
    return message;
}
std::string NetworkException::GetMessageStdString()
{
    return message.toStdString();
}
NetworkException::NetworkException()
{

}
