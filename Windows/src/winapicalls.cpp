#include "winapicalls.h"

WinApiCalls::WinApiCalls()
{

}

std::string WinApiCalls::GetWindowsVersion()
{
        if(IsWindows10OrGreater())
        {
        return  "10" ;
        }
        if (IsWindows8Point1OrGreater())
        {
            return "8";
        }
        if (IsWindows8OrGreater())
        {
            return "8";
        }
        if (IsWindows7OrGreater())
        {
            return "7";
        }
        if (IsWindowsVistaOrGreater())
        {
            return "vista";
        }

        if(IsWindowsXPOrGreater())
        {
            return  "xp";
        }
        return "unknown";

}

void WinApiCalls::GlassEffect(int *WinId)
{
    HMODULE dwm = ::LoadLibrary( L"dwmapi.dll" );
    if(dwm)
    {
        typedef HRESULT (WINAPI *pDwmExtendFrameIntoClientArea)(HWND, void * MARGINS);
        pDwmExtendFrameIntoClientArea procAddr = (pDwmExtendFrameIntoClientArea)::GetProcAddress(dwm, "DwmExtendFrameIntoClientArea");
        if(procAddr)
        {
            MARGINS a = {0,0,1000,0};
            HRESULT hr = (procAddr) ((HWND)WinId,&a);
        }
        ::FreeLibrary(dwm);
    }
};

