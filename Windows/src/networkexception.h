#ifndef NETWORKEXCEPTION_H
#define NETWORKEXCEPTION_H
#include <QtNetwork>

class NetworkException
{
public:
    NetworkException(QNetworkReply *rep);
    NetworkException();
    QString GetMessageQString();
    std::string GetMessageStdString();
private:
    QString message;
    QString code;
};

#endif // NETWORKEXCEPTION_H
