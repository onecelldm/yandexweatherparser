#ifndef WINAPICALLS_H
#define WINAPICALLS_H
#include<dwmapi.h>
#include <versionhelpers.h>
#include <iostream>
class WinApiCalls
{
public:
    WinApiCalls();
    void GlassEffect(int *WindowId);
    std::string GetWindowsVersion();


};

#endif // WINAPICALLS_H
