#ifndef NETWORK_H
#define NETWORK_H
#include <QtNetwork>
#include <QObject>
#include <networkexception.h>
class Network : public QObject
{
    Q_OBJECT
public:
    explicit Network(QObject *parent = nullptr);
    QByteArray GetData(QString url);
    QString Download(QString url, QString filename);
    bool CheckConnection(QString url);
    QString GetForwardUrl(QString url);
signals:

public slots:
};

#endif // NETWORK_H
