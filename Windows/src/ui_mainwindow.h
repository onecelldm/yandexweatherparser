/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_5;
    QLabel *LabelNightTemp;
    QLabel *LabelNightImage;
    QVBoxLayout *verticalLayout_3;
    QLabel *LabelDayTemp;
    QLabel *LabelDeyImage;
    QLabel *LabelStatus;
    QVBoxLayout *verticalLayout_2;
    QLabel *LabelMorningTemp;
    QLabel *LabelMorningImage;
    QVBoxLayout *verticalLayout_4;
    QLabel *LabelEvenignTemp;
    QLabel *LabelEveningImage;
    QVBoxLayout *verticalLayout;
    QLabel *LabelCorrentTemp;
    QLabel *LabelCorrentImage;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->setWindowModality(Qt::ApplicationModal);
        MainWindow->resize(400, 313);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMaximumSize(QSize(400, 600));
        QFont font;
        font.setFamily(QString::fromUtf8("Noto Sans"));
        font.setPointSize(14);
        MainWindow->setFont(font);
        MainWindow->setMouseTracking(false);
        MainWindow->setTabletTracking(false);
        MainWindow->setLayoutDirection(Qt::LeftToRight);
        MainWindow->setAutoFillBackground(true);
        MainWindow->setToolButtonStyle(Qt::ToolButtonIconOnly);
        MainWindow->setAnimated(true);
        MainWindow->setDocumentMode(true);
        MainWindow->setTabShape(QTabWidget::Triangular);
        MainWindow->setDockNestingEnabled(true);
        MainWindow->setDockOptions(QMainWindow::AllowNestedDocks|QMainWindow::AnimatedDocks);
        MainWindow->setUnifiedTitleAndToolBarOnMac(true);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        LabelNightTemp = new QLabel(centralwidget);
        LabelNightTemp->setObjectName(QString::fromUtf8("LabelNightTemp"));

        verticalLayout_5->addWidget(LabelNightTemp);

        LabelNightImage = new QLabel(centralwidget);
        LabelNightImage->setObjectName(QString::fromUtf8("LabelNightImage"));

        verticalLayout_5->addWidget(LabelNightImage);


        gridLayout->addLayout(verticalLayout_5, 6, 1, 1, 1);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        LabelDayTemp = new QLabel(centralwidget);
        LabelDayTemp->setObjectName(QString::fromUtf8("LabelDayTemp"));

        verticalLayout_3->addWidget(LabelDayTemp);

        LabelDeyImage = new QLabel(centralwidget);
        LabelDeyImage->setObjectName(QString::fromUtf8("LabelDeyImage"));

        verticalLayout_3->addWidget(LabelDeyImage);


        gridLayout->addLayout(verticalLayout_3, 2, 1, 1, 1);

        LabelStatus = new QLabel(centralwidget);
        LabelStatus->setObjectName(QString::fromUtf8("LabelStatus"));

        gridLayout->addWidget(LabelStatus, 0, 0, 1, 2, Qt::AlignHCenter|Qt::AlignTop);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        LabelMorningTemp = new QLabel(centralwidget);
        LabelMorningTemp->setObjectName(QString::fromUtf8("LabelMorningTemp"));

        verticalLayout_2->addWidget(LabelMorningTemp);

        LabelMorningImage = new QLabel(centralwidget);
        LabelMorningImage->setObjectName(QString::fromUtf8("LabelMorningImage"));

        verticalLayout_2->addWidget(LabelMorningImage);


        gridLayout->addLayout(verticalLayout_2, 2, 0, 1, 1);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        LabelEvenignTemp = new QLabel(centralwidget);
        LabelEvenignTemp->setObjectName(QString::fromUtf8("LabelEvenignTemp"));

        verticalLayout_4->addWidget(LabelEvenignTemp);

        LabelEveningImage = new QLabel(centralwidget);
        LabelEveningImage->setObjectName(QString::fromUtf8("LabelEveningImage"));

        verticalLayout_4->addWidget(LabelEveningImage);


        gridLayout->addLayout(verticalLayout_4, 6, 0, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        LabelCorrentTemp = new QLabel(centralwidget);
        LabelCorrentTemp->setObjectName(QString::fromUtf8("LabelCorrentTemp"));

        verticalLayout->addWidget(LabelCorrentTemp);

        LabelCorrentImage = new QLabel(centralwidget);
        LabelCorrentImage->setObjectName(QString::fromUtf8("LabelCorrentImage"));

        verticalLayout->addWidget(LabelCorrentImage, 0, Qt::AlignHCenter);


        gridLayout->addLayout(verticalLayout, 1, 0, 1, 2);

        MainWindow->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "YndexWidget", nullptr));
        LabelNightTemp->setText(QApplication::translate("MainWindow", "\320\235\320\276\321\207\321\214\321\216", nullptr));
        LabelNightImage->setText(QString());
        LabelDayTemp->setText(QApplication::translate("MainWindow", "\320\224\320\275\320\265\320\274", nullptr));
        LabelDeyImage->setText(QString());
        LabelStatus->setText(QApplication::translate("MainWindow", "\320\241\321\202\320\260\321\202\321\203\321\201 ", nullptr));
        LabelMorningTemp->setText(QApplication::translate("MainWindow", "\320\243\321\202\321\200\320\276\320\274 ", nullptr));
        LabelMorningImage->setText(QString());
        LabelEvenignTemp->setText(QApplication::translate("MainWindow", "\320\222\320\265\321\207\320\265\321\200\320\276\320\274", nullptr));
        LabelEveningImage->setText(QString());
        LabelCorrentTemp->setText(QApplication::translate("MainWindow", "\320\241\320\265\320\271\321\207\320\260\321\201", nullptr));
        LabelCorrentImage->setText(QApplication::translate("MainWindow", "\320\235\320\265\321\202 \320\270\320\267\320\276\320\261\321\200\320\260\320\266\320\265\320\275\320\270\321\217", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
