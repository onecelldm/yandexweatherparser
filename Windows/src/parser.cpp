#include "parser.h"

Parser::Parser()
{}

QString Parser::CityNameParser(QByteArray response)
{
    QString ufo,str,substr;
        QStringList list;
        int start,stop;

        str=QString::fromUtf8(response);

        start = str.indexOf("header-title");
        stop=str.indexOf("footer iX0ib5JXo70Y");
        substr=str.mid(start, stop - start);
        list=substr.split("<");

        for(int i=0; i<list.count(); i++)
        {
            if(list.at(i).contains("title title_level_1 header-title__title"))
            {
                return list.at(i).split(">")[1];

            }
        }
}
std::map<QString,QStringList> Parser::citiesParser(QByteArray response)
{
            QString ufo,str,substr;
            QStringList list,titles,urls;
            std::map<QString,QStringList> dataMap;

            str=QString::fromUtf8(response);

            list=str.split("<");

            for(int i=0; i<list.count(); i++)
            {

                if(list.at(i).contains("link place-list__item-name i-bem"))
                {


                   urls.push_back("https://yandex.ru"+list.at(i).split("href=")[1].split("via")[0].remove("\\").remove('"').remove(";").remove("amp"));
                   titles.push_back( list.at(i).split(">")[1]);
                }
            }
            dataMap["titles"]=titles;
            dataMap["urls"]=urls;
            return dataMap;
}
/*QString Parser::UfoParser(QByteArray response)
{   QString ufo,str,substr;
    QStringList list;
    int start,stop;

    str=QString::fromStdString( response.toStdString());
    start = str.indexOf("details-celestial-bodies details-celestial-bodies_page_main card");
    stop=str.indexOf("fact__hourly fact__hourly_nav-visible_next i-bem fact__hourly_js_inited");
    substr=str.mid(start, stop - start);
    list=substr.split("<");
    for(int i=0; i<list.count(); i++)
    {
        if(list.at(i).contains("details-celestial-bodies__value"))
        {

        }
    }
}
*/
std::map<QString,QString>  Parser::CorrentDataParse(QByteArray response)
{


    QString temp="";
    QString image="";
    QString cloudness="";
    QString str=QString::fromStdString( response.toStdString());
    int start = str.indexOf("fact card card_size_big");
    int stop=str.indexOf("fact__hourly fact__hourly_nav-visible_next i-bem fact__hourly_js_inited");
    QString substr=str.mid(start, stop - start);

    QStringList list=substr.split("<");
            for(int i=0; i<list.count(); i++)
            {
                    if(list.at(i).contains("temp__value"))
                    {
                       QString data= list.at(i);
                       data=data.replace('"',"").replace("temp__value","").replace("<","").replace(">","").replace("/","").replace("span","").replace("class=","").replace("\\","").replace(" ","");;
                        temp=data;


                    }
                    if(list.at(i).contains("icon"))
                    {

                        QString data=list.at(i);

                        if(data.split("src=").count()>1)
                        {
                            data= data.split("src=")[1];
                            data=data.replace('"',"").replace("<","").replace(">","").replace("span","").replace("class=","").replace("src=","").replace("","").replace("img","").replace("icon icon_color_light icon_size_48 icon_thumb_ovc fact__icon","").replace(" ","");
                            data=data.remove(data.length()-1,1);
                            image=data;
                        }
                        else{

                            throw std::out_of_range ("Parser Error: Out of range");
                        }

                    }

                    if(list.at(i).contains("link__condition day-anchor i-bem"))
                    {

                        QString data=list.at(i);
                             data=data.replace('"',"").replace("<","").replace(">","").replace("/","").replace("class=","").replace("link__condition day-anchor i-bem","").replace("div","").replace(" ","");
                             if(data.split("'").count()>2){

                             data=data.split("'").at(2);
                             cloudness=data;
                             }
                             else{
                                 throw std::out_of_range ("Parser Error: Out of range");
                             }
                    }
                    if(temp!=""&image!=""&cloudness!="")
                    {
                        break;
                    }
            }
   std::map<QString,QString> mp;
   mp["temp"]=temp;
   mp["img"]="https:"+image;
   mp["cloudiness"]=cloudness;
   return mp;
  }





/*std::map<QString,QString> Parser::CloudParser(QByteArray response)
{
    std::map<QString,QString> mp;
    QStringList list,rowlist,data;
    QString _morning="",_night="",_evening="",_day="",buff,substr="",str,cloud;
    int start,stop,lastPos=0;
    QRegExp iconsRegExp("<tr class=\"weather-table__row\".*<td class=\"weather-table__body-cell weather-table__body-cell_type_condition\".*([^/td].+)<");

    str=QString::fromStdString( response.toStdString());
    start = str.indexOf("weather-table");
    stop=str.indexOf("forecast-details__right-column");
    substr=str.mid(start, stop - start);

    buff = substr;
    iconsRegExp.setMinimal(true);
    while ((lastPos = iconsRegExp.indexIn(buff, lastPos)) != -1) {
            lastPos += iconsRegExp.matchedLength();

            list.push_back( iconsRegExp.cap(0));
    }
    for(int day=3; day>=0; day--)
    {
       rowlist= list.at(day).split("<");
       for(int i=0; i<rowlist.count(); i++)
       {
            if(rowlist.at(i).contains("type_condition"))
             {
                data=rowlist.at(i).split(">");
                if(data.count()>2)
                {
                    cloud=data[1];
                }
                else{
                    throw std::out_of_range ("Parser Error: Out of range");
                }
            }
       }
       switch (day) {
        case 0: _morning=cloud; break;
        case 1: _day=cloud; break;
        case 2: _evening= cloud; break;
        case 3: _night= cloud; break;
       }

   }
    mp["morning"]=_morning;
    mp["day"]=_day;
    mp["evening"]=_evening;
    mp["night"]=_night;
    return mp;
}
*/
std::map<QString,QString> Parser::ImageParser(QByteArray response)
{
    std::map<QString,QString> mp;
    QStringList list,rowlist,data;
    QString _morningUrl="",_nightUrl="",_eveningUrl="",_dayUrl="",buff,substr="",str,url;
    int start,stop,lastPos=0;
    QRegExp iconsRegExp("<tr class=\"weather-table__row\".*src=\".*([/td].\)<");

    str=QString::fromStdString( response.toStdString());
    start = str.indexOf("weather-table");
    stop=str.indexOf("forecast-details__right-column");
    substr=str.mid(start, stop - start);

    buff = substr;
    iconsRegExp.setMinimal(true);
    while ((lastPos = iconsRegExp.indexIn(buff, lastPos)) != -1) {
            lastPos += iconsRegExp.matchedLength();
            //qDebug()<<iconsRegExp.cap(0);
            list.push_back( iconsRegExp.cap(0));


    }
    for(int day=3; day>=0; day--)
    {
       rowlist= list.at(day).split("<");
       for(int i=0; i<rowlist.count(); i++)
       {
           if(rowlist.at(i).contains("src="))
           {
               data=rowlist.at(i).split('=');
               if(data.count()>2)
               {
                    url=data[2].replace("\\","").replace(">","").replace('"',"");
                    url= url.remove(url.length()-1,1);
               }
               else{
                   throw std::out_of_range ("Parser Error: Out of range");
               }

           }
       }
       switch (day) {
        case 0: _morningUrl=url; break;
        case 1: _dayUrl=url; break;
        case 2: _eveningUrl= url; break;
        case 3: _nightUrl= url; break;
       }

   }

    mp["morning"]="https:"+_morningUrl;
    mp["day"]="https:"+_dayUrl;
    mp["evening"]="https:"+_eveningUrl;
    mp["night"]="https:"+_nightUrl;
    return mp;
}
std::map<QString,QString>  Parser::TempParser(QByteArray response)
{
    std::map<QString,QString> mp;
    QStringList list,rowlist,data;
    QString _morning="",_night="",_evening="",_day="",buff,substr="",str;
    int start,stop,lastPos=0;
    QRegExp iconsRegExp("<tr class=\"weather-table__row\".*<span class=\"temp__value\".*([/td].\)<");

    str=QString::fromStdString( response.toStdString());
    start = str.indexOf("weather-table");
    stop=str.indexOf("forecast-fields");
    substr=str.mid(start, stop - start);

    buff = substr;
    iconsRegExp.setMinimal(true);
    while ((lastPos = iconsRegExp.indexIn(buff, lastPos)) != -1) {
            lastPos += iconsRegExp.matchedLength();
            list.push_back( iconsRegExp.cap(0));
    }
    for(int day=3; day>=0; day--)
    {
       int iz=0;
       QString tempMin, tempMax;

         rowlist= list.at(day).split("<");

       for(int i=0; i<rowlist.count(); i++)
       {
           if(rowlist.at(i).contains("temp__value"))
           {
                data=rowlist.at(i).split('>');
                if(data.count()>1)
                {
                    if(iz==0)
                    {

                    tempMin=  data[1];
                    iz++;
                    }
                    else tempMax=data[1];
                }
                else{
                    throw std::out_of_range ("Parser Error: Out of range");
                }
            }
       }
       switch (day) {
        case 0: _morning=tempMax+" "+tempMin; break;
        case 1: _day=tempMax+" "+tempMin; break;
        case 2: _evening= tempMax+" "+tempMin; break;
        case 3: _night= tempMax+" "+tempMin; break;
       }
   }
   mp["morning"]=_morning;
   mp["day"]=_day;
   mp["evening"]=_evening;
   mp["night"]=_night;
   return mp;

}


