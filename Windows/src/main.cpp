#include "mainwindow.h"

#include <QApplication>



int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    QCoreApplication::setOrganizationName("seld");
    QCoreApplication::setOrganizationDomain("seld");
    QCoreApplication::setApplicationName("app");
    qDebug()<<  QCoreApplication::libraryPaths();
    MainWindow w;

    w.show();
    return a.exec();
}

