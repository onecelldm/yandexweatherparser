#ifndef PARSER_H
#define PARSER_H


#include <QStringList>
#include <qdebug.h>
#include  <QString>
#include <map>
#include <exception>

class Parser
{
public:
    Parser();

    std::map<QString,QString>  CorrentDataParse(QByteArray response);

    std::map<QString,QString>  TempParser(QByteArray response);
    std::map<QString,QString>  ImageParser(QByteArray response);
    std::map<QString,QString>  CloudParser(QByteArray response);
    QString  UfoParser(QByteArray response);
    QString  SunsetTimeParse(QByteArray response);
    QString  RisingTimeParse(QByteArray response);
    QString  CityNameParser(QByteArray response);
    std::map<QString,QStringList>  citiesParser(QByteArray response);
};

#endif // PARSER_H
